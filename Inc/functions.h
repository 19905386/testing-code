/*
 * functions.h
 *
 *  Created on: 20 Apr 2019
 *      Author: 20342853
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include "main.h"

int check_message_type(char *buffer, int length);
int	extract_time_of_day(char *buffer, int length);
int	extract_char_string(char *buffer, int length,char condition);
int comma_counter(char *buffer, int length, char condition);
int process_altitude_message(char *buffer, int length);
void process_latitude_message(char *buffer, int length);
void process_longitude_message(char *buffer, int length);
int check_checksum(char *buf,int length);
int burn_checker(double longitude_signal, double altitue_signal);
void main_if_statement();
void UART_message();
//****************************************************************
//following functions are from the mentioned websites
//void reverse(char *str, int len);//http://www.geeksforgeeks.org/convert-floating-point-number-string/
//int intToStr(int x, char str[], int d);//http://www.geeksforgeeks.org/convert-floating-point-number-string/
//void ftoa(float n, char *res, int afterpoint);//http://www.geeksforgeeks.org/convert-floating-point-number-string/
//

#endif /* FUNCTIONS_H_ */
