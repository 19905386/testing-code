/*
 * LCDFunctions.h
 *
 *  Created on: 24 Apr 2019
 *      Author: 19905386
 */

#ifndef LCDFUNCTIONS_H_
#define LCDFUNCTIONS_H_

#include<stdio.h>
#include<strings.h>
#include<stdlib.h>
#include "main.h"

#define LCDEnPin 10
#define LCDEnPort GPIOA
#define LCDRNWPin 9
#define LCDRNWPort GPIOA
#define LCDRSPin 8
#define LCDRSPort GPIOA
#define LCDD4Pin 9
#define LCDD4Port GPIOC
#define LCDD5Pin 8
#define LCDD5Port GPIOC
#define LCDD6Pin 7
#define LCDD6Port GPIOC
#define LCDD7Pin 6
#define LCDD7Port GPIOC
#define TimeDelayBeforeEnable 4000
#define TimeDelayBeforeDisable 8000
#define burnlocation 0xC1
#define templocation 0xC5
#define homelocation 0x80
#define cleardisplay 0x01

void LCDInitialization();
void LCDSetDisplay();
void LCDSendAnInstruction(char character);
void SendBitToPortAndPin(GPIO_TypeDef *port, int pinNumber,uint8_t BitState);
void SendAByteToTheLCDDataPins(char character);
void SetPortAndPinForOutput(GPIO_TypeDef *port, int pinNumber);
void InitializePortsForLCD();
void notExactTimeDelay(int timeDelay);
void LCDEnable();
void LCDSetToWrite();
void LCDSetToRead();
void LCDInstructionMode();
void LCDCharacterMode();
void LCDSendACharacter(char character);
void LCDSendAString(char *StringOfCharacters);
void LCDDisplayLeftShift();
void LCDDisplayRightShift();
void LCDSendAString(char *StringOfCharacters);
void LCDSendAnInteger(int IntegerToBeDisplayed, uint8_t length);
void LCDSendAFloat(float FloatToBeDisplayed, uint8_t MaxLenghtOfDigits);
//void LCDClearDisplay();
//void LCDSetDisplayandCursorHome();
//void LCDSetCursorBurnLocation();
//void LCDSetCursorTempLocation();
void LCDSetCursorLocation(char instruction);

#endif /* LCDFUNCTIONS_H_ */
