/*
 * BMEFunctions.h
 *
 *  Created on: 08 May 2019
 *      Author: 19905386
 */

#ifndef BMEFUNCTIONS_H_
#define BMEFUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include "main.h"
#include "bme280.h"

extern I2C_HandleTypeDef hi2c1;
extern int pressure_kPa;
extern int temperature_degC;
extern int humidity_relative;
extern int8_t rslt;


void BME280_init(void);
void user_delay_ms(uint32_t delay);
int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);
int8_t stream_sensor_data_forced_mode(struct bme280_dev *dev);
int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev);
void print_sensor_data(struct bme280_data *comp_data);

#endif /* BMEFUNCTIONS_H_ */
