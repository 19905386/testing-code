/*
 * variables.h
 *
 *  Created on: 20 Apr 2019
 *      Author: 20342853
 */

#ifndef VARIABLES_H_
#define VARIABLES_H_

#include "main.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//MAIN Loop variables
extern uint32_t burn_time;
extern int burn;
extern int pos_start;
extern int pos_end;
extern volatile int on_time;
extern int burn_counter;
extern int array_index;
extern volatile int begin;
extern volatile double save_longitude;
extern volatile double save_altitude;

extern int old_time_ms;
extern volatile int get_time_ms;
extern int get_time_sec;
extern int time_diff_ms;
extern int msg_index;
extern volatile int begin_process;
extern int msgflag;

//CHAR Arrays********************************************************
extern char flag;
extern char msg;
extern char message_array[100];
extern char array_buffer[100];
extern char output[92];

extern char time_of_day[100];//Time-of-day from GPS (hours, minutes, seconds)
extern volatile int hour, min, sec;

extern char alt[100];//GPS altitude in m
extern volatile int altitude_int, altitude_decimal;


extern char lat[100];//GPS latitude in deg
extern volatile int latitude_int, latitude_decimal;
extern char lat_buffer[100];

extern char lon[100];//GPS longitude in deg
extern volatile int longitude_int, longitude_decimal;
extern char lon_buffer[100];
//CHAR Strings*******************************************************
extern char* stdnum;
extern char *tmp ;//Temperature in deg C
extern char *hum;//% relative humidity
extern char *prs;//Atmospheric pressure in kPa units
extern char *acc1;//Linear acceleration per axis, in 1/1000 g units
extern char *acc2;//Linear acceleration per axis, in 1/1000 g units
extern char *acc3;//Linear acceleration per axis, in 1/1000 g units
extern char *cur ;//Measured supply current in mA
extern char *vol ;//Measured supply voltage in V

extern volatile int lets_go;
extern volatile int end_start;
extern volatile int difference;
extern volatile int start;
extern volatile int burn_flag;

// ADC Variables
extern uint32_t voltage_buffer[4], current_sensor_buffer[4];
extern uint32_t PA0_total, PA1_total;
extern uint32_t PA0_average_voltage, PA1_average_voltage;
extern double PA0_scaled_voltage;
extern double PA0_actual_voltage,PA1_actual_voltage;
extern volatile int PA0_actual_voltage_int, PA0_actual_voltage_fraction;
extern volatile int PA1_current;
extern volatile int ADC_counter;


#endif /* VARIABLES_H_ */
