/*
 * LCDFunctions.c
 *
 *  Created on: 24 Apr 2019
 *      Author: 19905386
 */

#include "LCDFunctions.h"
#include "main.h"

volatile char character;

void LCDInitialization(){

	LCDSendAnInstruction(0b00110011);//Set to 8-bit mode and 1 line display
	LCDSendAnInstruction(0b00110010);//Set to 8-bit mode and 1 line display
	LCDSendAnInstruction(0b00100100);//Set to 4-bit mode and 1 line display
	LCDSendAnInstruction(0b00001000);
	LCDSendAnInstruction(0b00000001);
	LCDSendAnInstruction(0b00000110);
}

void LCDSetDisplay(){
	LCDSendAnInstruction(0x28);//Set to 4-bit mode and 1 line display
	LCDSendAnInstruction(0x0E);
//	LCDSetDisplayandCursorHome();
	LCDSendAnInstruction(cleardisplay);
//	LCDClearDisplay();
}

void SetPortAndPinForOutput(GPIO_TypeDef *port, int pinNumber){


	if(port == GPIOA){
		RCC ->AHBENR |= RCC_AHBENR_GPIOAEN;
	}
	if(port == GPIOB){
		RCC ->AHBENR |= RCC_AHBENR_GPIOBEN;
	}
	if(port == GPIOC){
		RCC ->AHBENR |= RCC_AHBENR_GPIOCEN;
	}
	if(port == GPIOD){
		RCC ->AHBENR |= RCC_AHBENR_GPIODEN;
	}

	port ->MODER &= ~(1<< ((pinNumber*2)+1));
	port->MODER |= (1 << pinNumber*2);

	port ->OTYPER &= ~(1 << pinNumber);

	port ->OSPEEDR |= (1 << ((pinNumber*2)+1)) | (1<< pinNumber*2);

	port->PUPDR &= ~(1 << pinNumber);
}

void SendBitToPortAndPin(GPIO_TypeDef *port, int pinNumber,uint8_t BitState){
	if(BitState){
		port ->BSRR |= (1 << pinNumber);
	}else{
		port ->BRR |= (1 << pinNumber);
	}


}



void SendAByteToTheLCDDataPins(char character){

	SendBitToPortAndPin(LCDD4Port, LCDD4Pin,character & 0b0001);
	SendBitToPortAndPin(LCDD5Port, LCDD5Pin,character & 0b0010);
	SendBitToPortAndPin(LCDD6Port, LCDD6Pin,character & 0b0100);
	SendBitToPortAndPin(LCDD7Port, LCDD7Pin,character & 0b1000);

	notExactTimeDelay(TimeDelayBeforeDisable);
	SendBitToPortAndPin(LCDEnPort, LCDEnPin,0);

}

void InitializePortsForLCD(){
	SetPortAndPinForOutput(LCDD4Port,LCDD4Pin);
	SetPortAndPinForOutput(LCDD5Port,LCDD5Pin);
	SetPortAndPinForOutput(LCDD6Port,LCDD6Pin);
	SetPortAndPinForOutput(LCDD7Port,LCDD7Pin);

	SetPortAndPinForOutput(LCDEnPort,LCDEnPin);
	SetPortAndPinForOutput(LCDRNWPort,LCDRNWPin);
	SetPortAndPinForOutput(LCDRSPort,LCDRSPin);
}

void notExactTimeDelay(int timeDelay){
	volatile int i;
	for(i = 0; i < timeDelay; i++){

	}
}

void LCDEnable(){
	notExactTimeDelay(TimeDelayBeforeEnable);
	SendBitToPortAndPin(LCDEnPort, LCDEnPin,1);
}

void LCDSetToWrite(){

	SendBitToPortAndPin(LCDRNWPort, LCDRNWPin,0);
}

void LCDSetToRead(){
	SendBitToPortAndPin(LCDRNWPort, LCDRNWPin,1);
}

void LCDInstructionMode(){
	SendBitToPortAndPin(LCDRSPort, LCDRSPin,0);
	LCDEnable();
}

void LCDCharacterMode(){
	SendBitToPortAndPin(LCDRSPort, LCDRSPin,1);
	LCDEnable();
}

void LCDSendAnInstruction(char character){
	LCDSetToWrite();
	LCDInstructionMode();
	LCDEnable();
	SendAByteToTheLCDDataPins(character>>4);
	LCDEnable();
	SendAByteToTheLCDDataPins(character);
}

void LCDSendACharacter(char character){
	LCDSetToWrite();
	LCDCharacterMode();
	LCDEnable();
	SendAByteToTheLCDDataPins(character>>4);
	LCDEnable();
	SendAByteToTheLCDDataPins(character);
}

void LCDSendAString(char *StringOfCharacters){
	int i =0;
	while(*StringOfCharacters){
		LCDSendACharacter(*StringOfCharacters++);
		if(i == 7){

			LCDSendAnInstruction(0b11000000);//Sets Display Address to beginning of second line
		}
		i++;
	}
}

void LCDDisplayLeftShift(){
	LCDSendAnInstruction(0b00011000);
}

void LCDDisplayRightShift(){
	LCDSendAnInstruction(0b00011100);
}

void LCDSendAnInteger(int IntegerToBeDisplayed, uint8_t MaxLenghtOfDigits){
	char StringNumber[MaxLenghtOfDigits];
	sprintf(StringNumber, "%d", IntegerToBeDisplayed);

	LCDSendAString(StringNumber);
}

void LCDSendAFloat(float FloatToBeDisplayed, uint8_t MaxLenghtOfDigits){
	char StringNumber[MaxLenghtOfDigits];
	snprintf(StringNumber,MaxLenghtOfDigits + 1, "%f", FloatToBeDisplayed);

	LCDSendAString(StringNumber);
}

//void LCDClearDisplay(){
//	LCDSendAnInstruction(0b00000001);
//}

void LCDSetCursorLocation(char instruction){
	LCDSendAnInstruction(instruction);
}

void LCDSetDisplayandCursorHome(){
	LCDSendAnInstruction(0b00001110);// Turn on display and cursor
}


