/*
 * variables.c
 *
 *  Created on: 20 Apr 2019
 *      Author: 20342853
 */

#include "main.h"
#include "variables.h"

//MAIN Loop variables
int burn_counter;
volatile double save_longitude;
volatile double save_altitude;
uint32_t burn_time=0;
int burn = 0;
int pos_start = 0;
int pos_end = 0;
int array_index=0;
int msg_index = 0;
volatile int begin_process = 0;
volatile int begin = 0;

int old_time_ms=0;
volatile int get_time_ms;
int get_time_sec;
int time_diff_ms;
int get_burn_time = 1;

int msgflag=1;

//CHAR Arrays********************************************************
char flag;
char msg;
char message_array[100];
char array_buffer[100];
char output[92];

char time_of_day[100];//Time-of-day from GPS (hours, minutes, seconds)
volatile int hour, min, sec;

char alt[100];//GPS altitude in m
volatile int altitude_int, altitude_decimal;


char lat[100];//GPS latitude in deg
volatile int latitude_int, latitude_decimal;
char lat_buffer[100];

char lon[100];//GPS longitude in deg
volatile int longitude_int, longitude_decimal;
char lon_buffer[100];

//CHAR Strings*******************************************************
char* stdnum = "$19905386,";
char *tmp =",  0" ;//Temperature in deg C
char *hum =",  0";//% relative humidity
char *prs =",  0";//Atmospheric pressure in kPa units
char *acc1= ",   0";//Linear acceleration per axis, in 1/1000 g units
char *acc2= ",   0";//Linear acceleration per axis, in 1/1000 g units
char *acc3= ",   0";//Linear acceleration per axis, in 1/1000 g units
char *cur = ",  0";//Measured supply current in mA
char *vol = ",  0\n";//Measured supply voltage in V

volatile int lets_go = 0;
volatile int end_start = 0;
volatile int difference = 0;
volatile int start = 0;
volatile int burn_flag = 0;
volatile int on_time = 0;
//ADC variables
uint32_t voltage_buffer[4], current_sensor_buffer[4];
uint32_t PA0_average_voltage, PA1_average_voltage;
double PA0_scaled_voltage;
double PA0_actual_voltage,PA1_actual_voltage;
volatile int ADC_counter = 0;
uint32_t PA0_total = 0;
uint32_t PA1_total = 0;
volatile int PA1_current;
volatile int PA0_actual_voltage_int = 0;
volatile int PA0_actual_voltage_fraction = 0;
