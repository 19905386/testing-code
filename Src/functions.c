/*
 * functions.c
 *
 *  Created on: 20 Apr 2019
 *      Author: 20342853
 */

#include "main.h"
#include "variables.h"
#include "functions.h"
#include "LCDFunctions.h"
#include "BMEFunctions.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

extern int accx,accy,accz;
extern struct bme280_dev dev;
extern struct bme280_data comp_data;

void UART_message(){

	if(msgflag==1){
		sprintf(output,"$19905386,% 5d,00:00:00,%3d,%3d,%3d,%4d,%4d,%4d,         0,          0,      0,%3d,%1d.%1d\n",get_time_sec,temperature_degC,humidity_relative,pressure_kPa,accx,accy,accz,PA1_current,PA0_actual_voltage_int,PA0_actual_voltage_fraction);
	}
	else{
		sprintf(output,"$19905386,% 5d,%02d:%02d:%02d,%3d,%3d,%3d,%4d,%4d,%4d,%3d.%06d,%4d.%06d,%5d.%01d,%3d,%1d.%1d\n",get_time_sec,hour,min,sec,temperature_degC,humidity_relative,pressure_kPa,accx,accy,accz,latitude_int,latitude_decimal,longitude_int,longitude_decimal,altitude_int,altitude_decimal,PA1_current,PA0_actual_voltage_int,PA0_actual_voltage_fraction);
	}

	HAL_UART_Transmit(&huart1,(uint8_t*)output,strlen(output),1000);

	old_time_ms = get_time_ms;
	on_time;

	if((on_time == 10000) && (burn_flag == 1)){
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,RESET);
		burn_flag = 0;
		burn = 0;
		burn_counter = 0;
		on_time = 0;
	}

	LCDSendAnInstruction(cleardisplay);
	LCDSetCursorLocation(homelocation);
	LCDSendAnInteger(altitude_int,5);
	LCDSendACharacter('m');
	if(burn_flag == 1){
		LCDSetCursorLocation(burnlocation);
		LCDSendACharacter('B');
	}
	LCDSetCursorLocation(templocation);
	LCDSendAnInteger(temperature_degC,2);
	LCDSendACharacter('C');
}

void main_if_statement(){


	if(begin == 1){ // if inside a valid string, copy char into message array
		msg_index = 0;
		for(int i = pos_start; i <= pos_end; i++){
			message_array[i] = array_buffer[i];
			msg_index++;
		}
		if(array_buffer[pos_end] == '\n'){ // check if not at end of message
			check_message_type(message_array,msg_index);
			begin_process=check_checksum(message_array,msg_index);
			begin = 0;
			for(int a = 0; a < 100 ; a++){
								array_buffer[a] = '\0';
							}
			array_index = 0;
		}
	}



	if((begin_process == 1)&&(lets_go == 1)){ // if received message at end, start processing message
		msgflag=0;

		extract_time_of_day(message_array,msg_index);
		process_latitude_message(message_array,msg_index);
		process_longitude_message(message_array,msg_index);
		process_altitude_message(message_array,msg_index);

		burn = burn_checker(save_longitude,save_altitude);
		if((burn == 1)&&(burn_counter == 5)){
			if(burn_counter == 5){
				burn_time = HAL_GetTick();
			}
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5,SET);
			burn_flag =1;
			LCDSetCursorLocation(burnlocation);
			LCDSendACharacter('B');
		}

		for(int k = 0; k < 100 ; k++){
			message_array[k] = '\0';
		}
		begin_process = 0;
		msg_index = 0;
		lets_go = 0;
		pos_start = 0;
	}



	flag = 0;
}

int check_message_type(char *buffer, int length){
	int step = 0; // stepper will step through array and sort everything out
	int equals_six_start = 0;
	for(step = 0; step <= length-3; step++){
		if((buffer[step] == '$') || (buffer[step] == 'G')|| (buffer[step] == 'P')|| (buffer[step] == 'A')){ // Check if it is a GPGGA message
			equals_six_start++; // if it is GPGGA message, continue processing
		}
	}

	if(equals_six_start == 6){
		lets_go = 1;
	}

	return lets_go;

}

int comma_counter(char *buffer, int length, char condition){
	int count = 0;
	int sum_of_comma = 0;
	int check = 0;
	end_start = 0;
	start = 0;


	switch(condition){
	case 'A':
		for(count = 1; count <= length ; count++){ // increment count counter
			if(buffer[count-1] == ','){
				sum_of_comma++;
			}

			if((sum_of_comma == 1)&&(check == 0)){
				start = count;
				check = 1;
			}

			if((sum_of_comma == 2)&&(check == 1)){
				end_start = count;
				check = 0;
			}
		}
		break;
	case 'B':
		for(count = 1; count <= length ; count++){ // increment count counter
			if(buffer[count-1] == ','){
				sum_of_comma++;
			}

			if((sum_of_comma == 9)&&(check == 0)){
				start = count;
				check = 1;
			}

			if((sum_of_comma == 10)&&(check == 1)){
				end_start = count-1;
				check = 0;
			}
		}
		break;
	case 'C':
		for(count = 1; count <= length ; count++){ // increment count counter
			if(buffer[count-1] == ','){
				sum_of_comma++;
			}

			if((sum_of_comma == 2)&&(check == 0)){
				start = count;
				check = 1;
			}

			if((sum_of_comma == 3)&&(check == 1)){
				end_start = count;
				check = 0;
			}
		}
		break;
	case 'D':
		for(count = 1; count <= length ; count++){ // increment count counter
			if(buffer[count-1] == ','){
				sum_of_comma++;
			}

			if((sum_of_comma == 4)&&(check == 0)){
				start = count;
				check = 1;
			}

			if((sum_of_comma == 5)&&(check == 1)){
				end_start = count;
				check = 0;
			}
		}
		break;
	case 'E':
	for(count = 1; count <= length ; count++){ // increment count counter
				if(buffer[count-1] == ','){
					sum_of_comma++;
				}

				if((sum_of_comma == 13)&&(check == 0)){
					start = count;
					check = 1;
				}

				if((sum_of_comma == 14)&&(check == 1)){
					end_start = count+1;
					check = 0;
				}
			}
			break;

	}
	return start,end_start;
}

int	extract_time_of_day(char *buffer, int length){

	int n=0;


	comma_counter(buffer,length, 'A');

	n = start;

	for(n = start; n < end_start-4; n++){
		time_of_day[n-start] = buffer[n];
	}

	hour= ((time_of_day[0]-'0')*10)+(time_of_day[1]-'0');
	min= ((time_of_day[2]-'0')*10)+(time_of_day[3]-'0');
	sec= ((time_of_day[4]-'0')*10)+(time_of_day[5]-'0');

	sprintf(time_of_day,"%c%c:%c%c:%c%c",time_of_day[0],time_of_day[1],time_of_day[2],time_of_day[3],time_of_day[4],time_of_day[5]);

	return *time_of_day;
}

int extract_char_string(char *buffer, int length, char condition){

	int n=0;
	switch(condition){
	case 'A':
		comma_counter(buffer,length, 'C');

		n = start;

		for(n = start; n <= end_start; n++){
			lat_buffer[n-start] = buffer[n];
		}
		return *lat_buffer;
		break;
	case 'B':
		comma_counter(buffer,length, 'D');

		n = start;

		for(n = start; n <= end_start; n++){
			lon_buffer[n-start] = buffer[n];
		}
		return *lon_buffer;
		break;
	}

	return 0;
}

int process_altitude_message(char *buffer, int length){
	int n = 0;
	char altitude_buffer[10];
	start = 0;
	end_start = 0;
	difference = 0;

	comma_counter(buffer,length, 'B');

	n = start;
	for(n = start; n <= end_start-1; n++){
			altitude_buffer[n-start] = buffer[n];
		}

	for(int o = 0; o < 8; o++){
		if(altitude_buffer[o] == '.'){
			alt[o] = altitude_buffer[o];
			o++;
			alt[o] = altitude_buffer[o];
			o++;
			if(altitude_buffer[o] == '0'){
				alt[o] = '\0'; o++;
			}
		}else{
			alt[o] = altitude_buffer[o];
		}
	}

	save_altitude = strtod(alt,'\0');
	altitude_int=(int)(save_altitude);
	altitude_decimal=(int)(((int)(save_altitude*100)%100)/10);
	return *alt;
}

void process_latitude_message(char *buffer, int length){
	char minutes_buffer[100] = {0};
	char degrees_buffer[100] = {0};
	double degrees = 0;
	double minutes;
	double converted_latitude = 0;
	int n = 0;

	start = 0;
	end_start = 0;

	extract_char_string(buffer,length,'A');
	comma_counter(buffer,length, 'C');

	difference = end_start - start;

	for(n = 0; n < difference ; n++){
		if(n <= 1){
			degrees_buffer[n] = lat_buffer[n]-'0';
		}else{
			minutes_buffer[n-2] = lat_buffer[n];
		}
	}

	degrees=(degrees_buffer[0]*10)+(degrees_buffer[1]);
	minutes = strtod(minutes_buffer,'\0');

	latitude_decimal=(int)((minutes/60)*1000000);

	converted_latitude = degrees + (minutes/60.0);

	if(lat_buffer[difference] == 'S'){

		latitude_int=(int)degrees *(-1);
	}else{

		latitude_int=(int)degrees *(1);
	}

}

void process_longitude_message(char *buffer, int length){
	char minutes_buffer[100] = {0};
	char degrees_buffer[100] = {0};
	double degrees = 0;
	double minutes;
	double converted_longitude = 0;
	int n = 0;
	start = 0;
	end_start = 0;

	extract_char_string(buffer,length,'B');
	comma_counter(buffer,length, 'D');

	difference = end_start - start;

	for(n = 0; n < difference ; n++){
		if(n<=2){
			degrees_buffer[n] = lon_buffer[n]-'0';
		}else{
			minutes_buffer[n-3] = lon_buffer[n];
		}
	}

	degrees=(degrees_buffer[0]*100)+(degrees_buffer[1]*10)+(degrees_buffer[2]);
	minutes = strtod(minutes_buffer,'\0');

	longitude_decimal=(int)((minutes/60)*1000000);

	converted_longitude = degrees + (minutes/60.0);
	save_longitude = converted_longitude;


	if(converted_longitude >= 100){
		if(lon_buffer[difference] == 'W'){
			longitude_int=(int)degrees *(-1);
		}else{
			longitude_int=(int)degrees *(1);
		}
	}else if((converted_longitude >= 10)&&(converted_longitude < 100)){
		if(lon_buffer[difference] == 'W'){
					longitude_int=(int)degrees *(-1);
				}else{
					longitude_int=(int)degrees *(1);
				}
	}else if(converted_longitude <10){
		if(lon_buffer[difference] == 'W'){
						longitude_int=(int)degrees *(-1);
					}else{
						longitude_int=(int)degrees *(1);
					}
	}

}


int check_checksum(char *buf,int length){
	//checksum is a hexidecimal value
	//xor all ascii values of input data before the *
	//check if the result of the xor = to checksum from input
	//if equal, valid msg, else, invalid msg
	int j=0;
	char checksum[2];
	int c=0;//check
	char cs[2];
	comma_counter(buf,length,'E');

	checksum[0]=buf[(start+2)];
	checksum[1]=buf[(end_start+1)];

	for(j=1;j<=(start);j++){
		c=(c^buf[j]);
	}
	sprintf(cs,"%02X",c);



	if((cs[0]==checksum[0])&&(cs[1]==checksum[1])){
		return 1;
	}
	else{
		return 0;
	}

}


int burn_checker(double longitude_signal, double altitude_signal){


	if(((longitude_signal <= 17.976343)||(longitude_signal >= 18.9354))&&(altitude_signal >= 10000)){
		burn_counter++;
		return 1;
	}else{
		burn_counter=0;
		return 0;
	}




}

