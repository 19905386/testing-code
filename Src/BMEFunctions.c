/*
 * BMEFunctions.c
 *
 *  Created on: 08 May 2019
 *      Author: 19905386
 */
#include "main.h"
#include "BMEFunctions.h"
#include "LCDFunctions.h"
#include "stm32f3xx.h"

struct bme280_dev dev;
int pressure_kPa;
int temperature_degC;
int humidity_relative;
int8_t rslt;

void BME280_init(void)
{
	rslt = BME280_OK;

    dev.dev_id = BME280_I2C_ADDR_PRIM; //Address = 0X76 (SDO -> GND)
    dev.intf = BME280_I2C_INTF; //Set I2C Interface
    dev.read = user_i2c_read; //READ Function
    dev.write = user_i2c_write; //WRITE Function
    dev.delay_ms = user_delay_ms; //DELAY Function

    rslt = bme280_init(&dev);

}

void user_delay_ms(uint32_t delay)
{
	int first_tick = HAL_GetTick();

	while((HAL_GetTick() - first_tick) < delay)
	{
	}
}

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    /*
     * HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
     * HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Stop       | -                   |
     * | Start      | -                   |
     * | Read       | (reg_data[0])       |
     * | Read       | (....)              |
     * | Read       | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    HAL_I2C_Master_Transmit(&hi2c1, (dev_id<<1)&0xFE, &reg_addr, 1, 100);
    HAL_I2C_Master_Receive(&hi2c1, ( ((dev_id<<1)&0xFE) + 1 ), reg_data, len, 100);

    return rslt;
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */
	uint8_t user_i2c_buff[100];

	/*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Write      | (reg_data[0])       |
     * | Write      | (....)              |
     * | Write      | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */
	user_i2c_buff[0] = reg_addr;

    for(int i = 0; i < len; i++){
    user_i2c_buff[i+1] = reg_data[i];
    }



    HAL_I2C_Master_Transmit(&hi2c1, (dev_id<<1)&0xFE, (uint8_t*)user_i2c_buff, len+1, 100);


    return rslt;
}


int8_t stream_sensor_data_normal_mode(struct bme280_dev *dev)
{
	int8_t rslt;
	uint8_t settings_sel;
//	struct bme280_data comp_data;

	/* Recommended mode of operation: Indoor navigation */
//	dev->settings.osr_h = BME280_OVERSAMPLING_1X;//Settings for weather measurement
//	dev->settings.osr_p = BME280_NO_OVERSAMPLING;//Settings for weather measurement
//	dev->settings.osr_t = BME280_OVERSAMPLING_1X;//Settings for weather measurement
//	dev->settings.filter =BME280_FILTER_COEFF_OFF;//Settings for weather measurement
	dev->settings.osr_h = BME280_OVERSAMPLING_1X;//Settings for indoor navigations
	dev->settings.osr_p = BME280_OVERSAMPLING_16X;//Settings for indoor navigations
	dev->settings.osr_t = BME280_OVERSAMPLING_2X;//Settings for indoor navigations
	dev->settings.filter = BME280_FILTER_COEFF_16;//Settings for indoor navigations
	dev->settings.standby_time = BME280_STANDBY_TIME_0_5_MS;
//	dev->settings.standby_time = BME280_STANDBY_TIME_62_5_MS;

	settings_sel = BME280_OSR_PRESS_SEL	| BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_STANDBY_SEL | BME280_FILTER_SEL;
//	settings_sel |= BME280_OSR_TEMP_SEL;
//	settings_sel |= BME280_OSR_HUM_SEL;
//	settings_sel |= BME280_STANDBY_SEL;
//	settings_sel |= BME280_FILTER_SEL;
	rslt = bme280_set_sensor_settings(settings_sel, dev);
	rslt = bme280_set_sensor_mode(BME280_NORMAL_MODE, dev);

	//while (1) {
		/* Delay while the sensor completes a measurement */
		dev->delay_ms(70);
//		rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
//		print_sensor_data(&comp_data);

	return rslt;
}

void print_sensor_data(struct bme280_data *comp_data)
{
#ifdef BME280_FLOAT_ENABLE
			LCDSetCursorLocation(templocation);
			LCDSendAnInteger(comp_data->temperature,3);
			LCDSendACharacter('C');
//	        LCDSetCursorLocation(0x85);
	        pressure_kPa = comp_data->pressure;
	        pressure_kPa = (int)pressure_kPa/1000;
	        temperature_degC = (int)comp_data->temperature;
	        humidity_relative = (int)comp_data-> humidity;
//	        LCDSendAnInteger(pressure_kPa,3);
//	        LCDSetCursorLocation(burnlocation);
//	        LCDSendAString("kPa");
//	        LCDSetCursorLocation(templocation);
//	        LCDSendAnInteger(comp_data->humidity,3);

#else
#endif
}

